package com.ruoyi.service.service;

import java.util.List;
import com.ruoyi.service.domain.WdsShortcut;

/**
 * shortcutService接口
 * 
 * @author gary
 * @date 2024-01-13
 */
public interface IWdsShortcutService 
{
    /**
     * 查询shortcut
     * 
     * @param id shortcut主键
     * @return shortcut
     */
    public WdsShortcut selectWdsShortcutById(Long id);

    /**
     * 查询shortcut列表
     * 
     * @param wdsShortcut shortcut
     * @return shortcut集合
     */
    public List<WdsShortcut> selectWdsShortcutList(WdsShortcut wdsShortcut);

    /**
     * 新增shortcut
     * 
     * @param wdsShortcut shortcut
     * @return 结果
     */
    public int insertWdsShortcut(WdsShortcut wdsShortcut);

    /**
     * 修改shortcut
     * 
     * @param wdsShortcut shortcut
     * @return 结果
     */
    public int updateWdsShortcut(WdsShortcut wdsShortcut);

    /**
     * 批量删除shortcut
     * 
     * @param ids 需要删除的shortcut主键集合
     * @return 结果
     */
    public int deleteWdsShortcutByIds(Long[] ids);

    /**
     * 删除shortcut信息
     * 
     * @param id shortcut主键
     * @return 结果
     */
    public int deleteWdsShortcutById(Long id);
}
