package com.ruoyi.service.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.service.mapper.WdsShortcutMapper;
import com.ruoyi.service.domain.WdsShortcut;
import com.ruoyi.service.service.IWdsShortcutService;

/**
 * shortcutService业务层处理
 * 
 * @author gary
 * @date 2024-01-13
 */
@Service
public class WdsShortcutServiceImpl implements IWdsShortcutService 
{
    @Autowired
    private WdsShortcutMapper wdsShortcutMapper;

    /**
     * 查询shortcut
     * 
     * @param id shortcut主键
     * @return shortcut
     */
    @Override
    public WdsShortcut selectWdsShortcutById(Long id)
    {
        return wdsShortcutMapper.selectWdsShortcutById(id);
    }

    /**
     * 查询shortcut列表
     * 
     * @param wdsShortcut shortcut
     * @return shortcut
     */
    @Override
    public List<WdsShortcut> selectWdsShortcutList(WdsShortcut wdsShortcut)
    {
        return wdsShortcutMapper.selectWdsShortcutList(wdsShortcut);
    }

    /**
     * 新增shortcut
     * 
     * @param wdsShortcut shortcut
     * @return 结果
     */
    @Override
    public int insertWdsShortcut(WdsShortcut wdsShortcut)
    {
        wdsShortcut.setCreateTime(DateUtils.getNowDate());
        return wdsShortcutMapper.insertWdsShortcut(wdsShortcut);
    }

    /**
     * 修改shortcut
     * 
     * @param wdsShortcut shortcut
     * @return 结果
     */
    @Override
    public int updateWdsShortcut(WdsShortcut wdsShortcut)
    {
        wdsShortcut.setUpdateTime(DateUtils.getNowDate());
        return wdsShortcutMapper.updateWdsShortcut(wdsShortcut);
    }

    /**
     * 批量删除shortcut
     * 
     * @param ids 需要删除的shortcut主键
     * @return 结果
     */
    @Override
    public int deleteWdsShortcutByIds(Long[] ids)
    {
        return wdsShortcutMapper.deleteWdsShortcutByIds(ids);
    }

    /**
     * 删除shortcut信息
     * 
     * @param id shortcut主键
     * @return 结果
     */
    @Override
    public int deleteWdsShortcutById(Long id)
    {
        return wdsShortcutMapper.deleteWdsShortcutById(id);
    }
}
