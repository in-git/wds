package com.ruoyi.service.mapper;

import java.util.List;
import com.ruoyi.service.domain.WdsShortcut;

/**
 * shortcutMapper接口
 * 
 * @author gary
 * @date 2024-01-13
 */
public interface WdsShortcutMapper 
{
    /**
     * 查询shortcut
     * 
     * @param id shortcut主键
     * @return shortcut
     */
    public WdsShortcut selectWdsShortcutById(Long id);

    /**
     * 查询shortcut列表
     * 
     * @param wdsShortcut shortcut
     * @return shortcut集合
     */
    public List<WdsShortcut> selectWdsShortcutList(WdsShortcut wdsShortcut);

    /**
     * 新增shortcut
     * 
     * @param wdsShortcut shortcut
     * @return 结果
     */
    public int insertWdsShortcut(WdsShortcut wdsShortcut);

    /**
     * 修改shortcut
     * 
     * @param wdsShortcut shortcut
     * @return 结果
     */
    public int updateWdsShortcut(WdsShortcut wdsShortcut);

    /**
     * 删除shortcut
     * 
     * @param id shortcut主键
     * @return 结果
     */
    public int deleteWdsShortcutById(Long id);

    /**
     * 批量删除shortcut
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWdsShortcutByIds(Long[] ids);
}
