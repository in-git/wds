package com.ruoyi.service.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.service.domain.WdsShortcut;
import com.ruoyi.service.service.IWdsShortcutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * shortcutController
 * 
 * @author gary
 * @date 2024-01-13
 */
@RestController
@RequestMapping("/service/shortcut")
public class WdsShortcutController extends BaseController
{
    @Autowired
    private IWdsShortcutService wdsShortcutService;

    /**
     * 查询shortcut列表
     */
//    @PreAuthorize("@ss.hasPermi('service:shortcut:list')")
    @GetMapping("/list")
    public TableDataInfo list(WdsShortcut wdsShortcut)
    {
        startPage();
        List<WdsShortcut> list = wdsShortcutService.selectWdsShortcutList(wdsShortcut);
        return getDataTable(list);
    }

    /**
     * 导出shortcut列表
     */
//    @PreAuthorize("@ss.hasPermi('service:shortcut:export')")
    @Log(title = "shortcut", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WdsShortcut wdsShortcut)
    {
        List<WdsShortcut> list = wdsShortcutService.selectWdsShortcutList(wdsShortcut);
        ExcelUtil<WdsShortcut> util = new ExcelUtil<WdsShortcut>(WdsShortcut.class);
        util.exportExcel(response, list, "shortcut数据");
    }

    /**
     * 获取shortcut详细信息
     */
//    @PreAuthorize("@ss.hasPermi('service:shortcut:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wdsShortcutService.selectWdsShortcutById(id));
    }

    /**
     * 新增shortcut
     */
//    @PreAuthorize("@ss.hasPermi('service:shortcut:add')")
    @Log(title = "shortcut", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WdsShortcut wdsShortcut)
    {
        return toAjax(wdsShortcutService.insertWdsShortcut(wdsShortcut));
    }

    /**
     * 修改shortcut
     */
//    @PreAuthorize("@ss.hasPermi('service:shortcut:edit')")
    @Log(title = "shortcut", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WdsShortcut wdsShortcut)
    {
        return toAjax(wdsShortcutService.updateWdsShortcut(wdsShortcut));
    }

    /**
     * 删除shortcut
     */
//    @PreAuthorize("@ss.hasPermi('service:shortcut:remove')")
    @Log(title = "shortcut", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wdsShortcutService.deleteWdsShortcutByIds(ids));
    }
}
