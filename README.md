## 温洞斯【预览版】

### 简介

- [x] 温洞斯是个别具一格的后端管理系统，它成功将 Web 与Windows 完美结合
- [x] 后端基于若依，具备若依的所有功能，业务逻辑跟若依完全一致
- [x] 窗口多开，拖拽，调整大小，告别传统单界面模式
- [x] 配置丰富，用户可以完全自定义表格的样式和整个界面的风格
- [x] 导入文件即可完成单表增删改查
- [x] 新的开发理念，参见**进阶**
- [x] 微功能预览
  - [ ] 在窗口中切换服务器，无需修改配置
  - [ ] 在窗口中查看请求/响应体
  - [ ] 动态表头，可显示/隐藏，本地存储
  - [ ] 多窗口拖拽分配权限【实验中】


#### 预览

- [x] 在线预览：http://in-git.gitee.io/wds
- [x] 若依官网：http://ruoyi.vip/
- [x] Arco UI：https://arco.design/vue/component/
- [ ] 注意：不要在https下打开本项目，否则无法连接到后端

#### 使用说明

- 前端使用Vue3+Ts+Arco实现，项目基于Arco Pro,首次初始化务必使用

```
pnpm i
```

- **如何新建项目**，上手Hello world

  - 新建一个菜单，名字为parent,代表父目录，在parent下新建一个子目录：child
  - 新建一个vue文件：src\views\modules\parent\children\index.vue，写上Hello world。注意，只能是index.vue,文件路径需要与菜单的路径对应。
  - 此时就能在桌面上双击打开child显示Hello world，完全具备窗口的拖拽，拉伸，缩小关闭，碰撞回弹的所有功能

- **如何进行增删改查**，只要做完下面几个填空题，就能拥有完整的增删改查页面

  ```vue
  <template>
    <ITemplate>
      <!-- 
        <template #actions="{ data }">
        右侧操作栏{{ data }}
        data是当前选中的那一列的值
      </template>
      <template #extraForm> 表格额外补充内容 </template>
      <template #form> 自定义表单弹窗插槽 </template>
      <template #operation> 显示隐藏列左侧的空位 </template> 
    -->
    </ITemplate>
  </template>
  
  <script setup lang="ts">
  import { IConfig } from "@/api/types";
  import ITemplate from "@/components/template/ITemplate.vue";
  import { ModuleEnum } from "@/data/modules";
  import { getConfig } from "@/data/template";
  import { Ref, provide } from "vue";
  
  /* any:和form的数据类型保持一致 */
  const config: Ref<IConfig<any>> = getConfig({
    // 必填,相当于ID,增删改查的核心，建议用对应表的ID
    rowKey: "",
    /**
     * 必填 ,卡片模式下显示的标题
      例如：你有一个用户表，其中用户名的字段是username，其中有个人叫张三
      建议用这个字段，卡片模式下就会显示：张三
    */
    cardKey: "",
    /* 
      必填，相当于整个模块的名字，可以用数据库的表名
      业务模块名，用于存储显示/隐藏的列名
    */
    moduleName: ModuleEnum.CONFIG,
    /* 填写增删改查接口，list必填 */
    api: {
      deleteList: undefined,
      update: undefined,
      create: undefined,
      select: undefined,
      list: undefined,
      export: undefined
    },
    /* 当前选中的那一项的所有信息，必填，否则不能编辑和新增 */
    form: undefined,
    /* 
    必填：表头信息否则不能显示数据
    表格和表单配置的结合体，在这里能够配置表单校验，具体使用参照已有的示例
    逻辑过于复杂建议新建页面
     */
    columns: []
  });
  
  provide("config", config.value);
  </script>
  
  ```

- **进阶**

  - 这种单表的增删改查，应用场景是非常有限的，高度集成的增删改查通用的组件太适合去修改，所以建议使用【**单表页面**】+ 【**需求页面**】的模式去实现逻辑。

  - **单表页面**即**温洞斯**自带的增删改查，**需求页面**是根据实际应用场景而专门开发的页面，二者互不干预，不在单表页面中写业务逻辑，也不在**需求页面**写单表增删改查

  - 可以通过权限进行设置，将**单表页面**对普通用户隐藏,超级管理员可见，仅开放需求页面

  - **需求页面**：在需求页面中，用户更希望能看到一些更友好的交互方式，可以用拖拽交互，Echarts，ThreeJs等各种技术现代化，可视化操作形式体现业务逻辑，想玩游戏一样，而不是去写作业。传统项目正是因为**单表逻辑**和**业务逻辑**完全混合，导致整个系统理解起来非常复杂。

  - 示例：图2的数据来源于**单表页面**，所以用户不需要填写任何内容，注意力完全聚焦在业务层，操作效率更高，也更容易理解

    <img src="./public/pro.webp">

    

### 后端部署

下载若依的后端部署即可完美对接，不需要修改一行代码，如果需要生成代码，可以下载本项目下backend的项目



### 项目截图

<img src="./public/dark.webp">

<img src="./public/light.webp">

<img src="./public/windows.webp">

<img src="./public/dev.webp">
