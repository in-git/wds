import { vitePluginForArco } from "@arco-plugins/vite-vue";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import { resolve } from "path";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { defineConfig } from "vite";
import { prismjsPlugin } from "vite-plugin-prismjs";
import configArcoStyleImportPlugin from "./plugin/arcoStyleImport";

export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    configArcoStyleImportPlugin(),
    vitePluginForArco({
      style: "css"
    }),
    AutoImport({
      // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
      imports: ["vue", "vue-router", "@vueuse/core"],
      dts: true,
      eslintrc: {
        enabled: true // <-- this
      }
    }),
    Components({
      include: [/\.vue$/, /\.vue\?vue/, /\.md$/],
      extensions: ["vue"],
      dts: "src/components.d.ts",
      dirs: ["./src/components"]
    }),
    prismjsPlugin({
      languages: ["js"], // 语言
      plugins: ["line-numbers", "show-language", "copy-to-clipboard", "inline-color"],
      theme: "okaidia", // 主题
      css: true
    })
  ],
  resolve: {
    alias: [
      {
        find: "@",
        replacement: resolve(__dirname, "../src")
      },
      {
        find: "assets",
        replacement: resolve(__dirname, "../src/assets")
      },
      // {
      //   find: 'vue-i18n',
      //   replacement: 'vue-i18n/dist/vue-i18n.cjs.js', // Resolve the i18n warning issue
      // },
      {
        find: "vue",
        replacement: "vue/dist/vue.esm-bundler.js" // compile template
      }
    ],
    extensions: [".ts", ".js"]
  },
  define: {
    "process.env": {},
    __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: "true"
  },
  css: {
    preprocessorOptions: {}
  },
  base: "/wds/"
});
