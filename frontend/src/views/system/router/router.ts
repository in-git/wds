import { ref } from "vue";
import { RouteRecordRaw } from "vue-router";

// eslint-disable-next-line import/prefer-default-export
export const routers = ref<RouteRecordRaw[]>([]);

export default {
  routers
};
