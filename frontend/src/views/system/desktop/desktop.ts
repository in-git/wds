import { currentWin } from "@/components/draggable/draggable";
import { getPageStore } from "@/store/page/utils";
import { nanoid } from "nanoid";
import { ref } from "vue";
import { GridConfig, GridItem } from "./types";

export type Mission = {
  component: any;
  name: string;
  id: string;
  show: boolean;
  /* 携带的属性 */
  attrs?: any;
  width: number;
  left: number;
  top: number;
  userSelect: "auto" | "none";
  height: number;
};

export const apps = ref<GridItem[]>([]);

export const missionList = ref<Mission[]>([]);

export const gridConfig = ref<GridConfig>({
  width: 64,
  height: 68,
  gap: 8
});

export const showMission = (id: string, flag = true) => {
  missionList.value.forEach((e) => {
    if (e.id === id) {
      e.show = flag;
    }
  });
};
export const editMission = (id: string, key: string, value: any) => {
  missionList.value.forEach((e: any) => {
    if (e.id === id) {
      e[key] = value;
    }
  });
};

/* 获取某个值 */
export const getMission = (id: string): Mission | undefined => {
  return missionList.value.find((e) => {
    if (e.id === id) {
      return e;
    }
    return null;
  });
};
/* 销毁任务 */
export const destroyMission = (id: string) => {
  missionList.value = missionList.value.filter((e) => e.id !== id);
};

type MissionParams = {
  attrs?: any;
  component: any;
  name: string;
  width?: number;
  height?: number;
};
/* 创建窗口任务 */
export const createMission = (mission: MissionParams) => {
  const { win } = getPageStore();

  const [w, h] = win.size.split(",");
  const offset = missionList.value.length * 10;
  const id = nanoid();

  const width = parseInt(w, 10);
  const height = parseInt(h, 10);
  const config: Mission = {
    id,
    component: mission.component,
    name: mission.name,
    show: true,
    attrs: mission.attrs,
    width,
    height,
    left: window.innerWidth / 2 - width / 2 + offset,
    top: window.innerHeight / 2 - height / 2 + offset,
    userSelect: "none"
  };

  currentWin.value = id;
  missionList.value.push(config);
};
