interface Meta {
  title: string;
  icon: string;
  noCache: boolean;
  link?: any;
}

export type TreeData = {
  name: string;
  children?: TreeData[];
  show?: boolean;
  meta: Meta;
  component: string;
  path: string;
};
