import { RouterChild } from "@/api/service/system/menu/types";

export type GridItem = {
  id: string;
  src: string;
  name: string;
  icon: any;
  type: "website" | "menu";
  data?: RouterChild;
  show?: boolean;
};
export type GridConfig = {
  width: number;
  height: number;
  gap: number;
};
