import { getRouters } from "@/api/service/system/menu/menu";
import { RouterChild } from "@/api/service/system/menu/types";
import { IconFolder } from "@arco-design/web-vue/es/icon";
import { nanoid } from "nanoid";
import { markRaw, ref } from "vue";
import { apps } from "../desktop/desktop";
import { GridItem } from "../desktop/types";

export const showUserCenter = ref(false);

export const systemRouters = ref<RouterChild[]>([]);

export default {
  showUserCenter
};
export const getApps = async () => {
  const { data } = await getRouters();
  if (!data.data) {
    return;
  }
  systemRouters.value = data.data;

  apps.value = [];
  data.data.forEach((e) => {
    if (e.hidden) {
      return;
    }
    const appItem: GridItem = {
      id: nanoid(),
      src: "",
      name: "",
      icon: "",
      type: "website",
      data: undefined
    };
    if (e.meta) {
      appItem.name = e.meta.title!;
      appItem.data = e;
    } else if (e.children) {
      appItem.name = e.children[0].meta!.title!;
      // eslint-disable-next-line prefer-destructuring
      appItem.data = e.children[0];
    }

    if (e.children) {
      appItem.type = "menu";
      appItem.icon = markRaw(IconFolder);
    } else {
      appItem.type = "website";
    }
    apps.value.push(appItem);
  });
};
