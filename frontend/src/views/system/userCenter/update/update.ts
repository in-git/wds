import { IUserInfo } from "@/api/service/system/user/types";
import { ref } from "vue";

export const passwordModal = ref({
  oldPassword: "",
  newPassword: "",
  confirmPassword: ""
});
export const userModal = ref<IUserInfo>({
  createBy: "",
  createTime: "",
  remark: "",
  userId: "",
  deptId: "",
  userName: "",
  nickName: "",
  email: "",
  phonenumber: "",
  sex: "",
  avatar: "",
  password: "",
  status: "",
  delFlag: "",
  loginIp: "",
  loginDate: "",
  dept: undefined,
  roles: [],
  admin: false,
  postIds: []
});
