import { RadioOption } from "@arco-design/web-vue/es/radio/interface";
import { ref } from "vue";

export const showSetting = ref(false);
export const sizeOption: RadioOption[] = [
  {
    label: "mini",
    value: "mini"
  },
  {
    label: "小",
    value: "small"
  },
  {
    label: "中",
    value: "medium"
  },
  {
    label: "大",
    value: "large"
  }
];
export const themeOption: RadioOption[] = [
  {
    label: "亮色",
    value: "light"
  },
  {
    label: "暗色",
    value: "dark"
  }
];
