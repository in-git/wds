import { ref } from "vue";

export interface RequestHistory {
  url: string;
  method: string;
  baseURL: string;
  data: any;
}

export type ResponseHistory = RequestHistory;
export const showLog = ref(false);
export const requestHistory = ref<RequestHistory[]>([]);
export const responseHistory = ref<ResponseHistory[]>([]);
