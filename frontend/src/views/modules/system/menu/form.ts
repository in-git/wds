import { IMenu } from "@/api/service/system/menu/types";
import { Columns } from "@/api/types";
import { isOrNotOptions, statusOptions } from "@/data/options";

export const menuColumns: Columns[] = [
  {
    dataIndex: "menuName",
    title: "菜单名",
    query: true,
    filedType: "input",
    isRequired: true
  },
  {
    dataIndex: "path",
    title: "路径",
    filedType: "input"
  },
  {
    dataIndex: "icon",
    title: "图标",
    filedType: "icon-selector"
  },
  {
    dataIndex: "orderNum",
    title: "排序",
    filedType: "number",
    isRequired: true
  },
  {
    dataIndex: "perms",
    title: "权限字符",
    filedType: "input",
    notColumn: true,
    tips: "菜单为按钮模式生效"
  },
  {
    dataIndex: "isFrame",
    title: "是否为链接",
    filedType: "switch",
    option: isOrNotOptions,
    isRequired: true
  },
  {
    dataIndex: "parentId",
    title: "父级ID",
    filedType: "treeSelect",
    treeOptions: [],
    isRequired: true,
    notColumn: true,
    fieldNames: {
      title: "menuName",
      key: "menuId",
      icon: "any"
    }
  },
  {
    dataIndex: "visible",
    title: "是否显示",
    filedType: "radio",
    option: isOrNotOptions,
    isRequired: true,
    notColumn: true
  },
  {
    dataIndex: "menuType",
    title: "菜单类型",
    filedType: "radio",
    option: [
      {
        label: "目录",
        value: "M"
      },
      {
        label: "菜单",
        value: "C"
      },
      {
        label: "按钮",
        value: "F"
      }
    ],
    isRequired: true
  },
  {
    dataIndex: "status",
    title: "状态",
    filedType: "radio",
    option: statusOptions,
    isRequired: true
  },
  {
    dataIndex: "actions",
    title: "操作",
    fixed: "right"
  }
];
export const menuForm: IMenu = {
  createTime: "",
  menuId: "",
  menuName: "",
  parentId: "0",
  orderNum: 0,
  path: "",
  query: "",
  isFrame: "1",
  isCache: "",
  menuType: "M",
  visible: "0",
  status: "0",
  perms: "",
  icon: "",
  children: []
};
