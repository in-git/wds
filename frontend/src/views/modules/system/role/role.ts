import { IMenu } from "@/api/service/system/menu/types";
import { Role } from "@/api/service/system/role/types";
import { ref } from "vue";

export const roleData = ref<Role[]>([]);

/* 当前选中的身份 */
export const currentRole = ref<Role>();

export const menuData = ref<IMenu[]>([]);

export const infoLoading = ref(false);
