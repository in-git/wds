import { Role } from "@/api/service/system/role/types";
import { Columns } from "@/api/types";

export const roleColumns: Columns[] = [
  {
    title: "身份名称",
    dataIndex: "roleName",
    filedType: "input",
    query: true,
    isRequired: true
  },
  {
    title: "权限标识",
    dataIndex: "roleKey",
    filedType: "input",
    isRequired: true
  },
  {
    title: "顺序",
    dataIndex: "roleSort",
    filedType: "number",
    isRequired: true
  },
  {
    title: "备注",
    dataIndex: "remark",
    filedType: "textarea"
  },
  {
    title: "菜单权限",
    dataIndex: "menuIds",
    filedType: "tree",
    notColumn: true,
    treeOptions: [],
    multiple: true,
    fieldNames: {
      title: "label",
      key: "id"
    }
  }
];
export const roleForm: Role = {
  roleId: "",
  roleName: "",
  roleKey: "",
  roleSort: 0,
  dataScope: "",
  menuCheckStrictly: false,
  deptCheckStrictly: false,
  status: "0",
  flag: false,
  admin: false
};
