import { IPost } from "@/api/service/system/post/types";
import { Columns } from "@/api/types";
import { statusOptions } from "@/data/options";

// eslint-disable-next-line import/prefer-default-export
export const postForm: IPost = {
  createBy: "",
  createTime: "",
  remark: "",
  postId: "",
  postCode: "",
  postName: "",
  postSort: 0,
  status: "0",
  flag: false
};
export const postColumns: Columns[] = [
  {
    title: "岗位名称",
    dataIndex: "postName",
    query: true,
    filedType: "input",
    isRequired: true
  },
  {
    title: "岗位编码",
    dataIndex: "postCode",
    filedType: "input"
  },
  {
    title: "排序",
    dataIndex: "postSort",
    allowSort: true,
    filedType: "number"
  },
  {
    title: "状态",
    dataIndex: "status",
    filedType: "radio",
    option: statusOptions
  },
  {
    title: "备注",
    dataIndex: "remark",
    filedType: "textarea"
  },
  {
    title: "创建时间",
    dataIndex: "createTime",
    allowSort: true
  }
];
