import { IUserInfo } from "@/api/service/system/user/types";
import { Columns } from "@/api/types";

export const userColumns: Columns[] = [
  {
    title: "昵称",
    dataIndex: "nickName",
    filedType: "input",
    query: true,
    isRequired: true
  },
  {
    title: "账号",
    dataIndex: "userName",
    query: true,
    filedType: "input",
    isRequired: true
  },
  {
    title: "Email",
    dataIndex: "email",
    filedType: "input",
    rules: {
      type: "email"
    }
  },
  {
    title: "岗位",
    dataIndex: "postIds",
    filedType: "select",
    option: [],
    multiple: true,
    isRequired: true,
    notColumn: true
  },
  {
    title: "手机号码",
    dataIndex: "phonenumber",
    query: true,
    filedType: "input",
    isRequired: true,
    rules: {
      maxLength: 11
    }
  },
  {
    title: "角色",
    dataIndex: "roleIds",
    notColumn: true,
    multiple: true,
    filedType: "select",
    option: [],
    fieldNames: {
      title: "roleName",
      key: "roleId"
    }
  },
  {
    title: "创建时间",
    dataIndex: "createTime",
    allowSort: true
  },
  {
    title: "部门",
    dataIndex: "deptId",
    filedType: "treeSelect",
    option: [],
    fieldNames: {
      title: "label",
      key: "id"
    }
  },
  {
    title: "状态",
    dataIndex: "status",
    filedType: "switch"
  },
  {
    title: "密码",
    dataIndex: "password",
    filedType: "password",
    notColumn: true
  },
  {
    title: "改密",
    dataIndex: "actions",
    notColumn: true
  }
];

export const userFormObject: IUserInfo = {
  createBy: "",
  createTime: "",
  remark: "",
  userId: "",
  deptId: "",
  userName: "",
  nickName: "",
  email: "",
  phonenumber: "",
  sex: "0",
  avatar: "",
  password: "",
  status: "0",
  delFlag: "",
  loginIp: "",
  loginDate: "",
  roles: [],
  admin: false,
  postIds: []
};
