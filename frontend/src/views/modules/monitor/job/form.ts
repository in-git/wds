import { Job } from "@/api/service/monitor/job/types";
import { Columns } from "@/api/types";
import { ref } from "vue";

export const jobForm = ref<Job>({
  createBy: "",
  createTime: "",
  remark: "",
  jobId: 0,
  jobName: "",
  jobGroup: "",
  invokeTarget: "",
  cronExpression: "",
  misfirePolicy: "",
  concurrent: "",
  status: "0",
  nextValidTime: ""
});
export const jobColumns: Columns[] = [
  {
    dataIndex: "jobName",
    title: "任务名"
  },
  {
    dataIndex: "jobGroup",
    title: "任务组"
  },
  {
    dataIndex: "cronExpression",
    title: "cron表达式"
  },
  {
    dataIndex: "status",
    title: "状态"
  }
];
