import { Online } from "@/api/service/monitor/online/types";
import { Columns } from "@/api/types";

// eslint-disable-next-line import/prefer-default-export
export const onlineColumns: Columns[] = [
  {
    title: "用户名",
    dataIndex: "userName",
    query: true,
    filedType: "input"
  },
  {
    title: "ip地址",
    dataIndex: "ipaddr",
    query: true
  },
  {
    title: "登录地址",
    dataIndex: "loginLocation"
  },
  {
    title: "浏览器",
    dataIndex: "browser"
  },
  {
    title: "登录时间",
    dataIndex: "loginTime",
    allowSort: true
  },
  {
    title: "操作时间",
    dataIndex: "os"
  },
  {
    title: "部门",
    dataIndex: "deptName"
  },
  {
    title: "操作",
    dataIndex: "actions"
  }
];
export const onlineForm: Online = {
  tokenId: "",
  deptName: "",
  userName: "",
  ipaddr: "",
  loginLocation: "",
  browser: "",
  os: "",
  loginTime: 0
};
