import { SelectOptionData, TableColumnData } from "@arco-design/web-vue";

export const htmlOption: SelectOptionData[] = [
  {
    label: "文本",
    value: "input"
  },
  {
    label: "文本域",
    value: "textarea"
  },
  {
    label: "下拉选项",
    value: "select"
  },
  {
    label: "布尔值",
    value: "boolean"
  },
  {
    label: "数字",
    value: "number"
  },
  {
    label: "单选",
    value: "radio"
  },
  {
    label: "多选",
    value: "checkbox"
  },
  {
    label: "时间选择器",
    value: "datetime"
  },
  {
    label: "图片上传",
    value: "imageUpload"
  },
  {
    label: "文本上传",
    value: "fileUpload"
  },
  {
    label: "富文本编辑器",
    value: "editor"
  }
];
export const staticColumns: TableColumnData[] = [
  {
    title: "列名",
    dataIndex: "columnName",
    width: 150
  },
  {
    title: "列类型",
    dataIndex: "columnType"
  },
  {
    title: "输入框类型",
    dataIndex: "htmlType",
    width: 200
  },
  {
    title: "必填",
    dataIndex: "isRequired"
  },
  {
    title: "插入",
    dataIndex: "isInsert"
  },
  {
    title: "编辑",
    dataIndex: "isEdit"
  },
  {
    title: "列表",
    dataIndex: "isList"
  },
  {
    title: "查询",
    dataIndex: "isQuery"
  }
];
