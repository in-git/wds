import { GenInfo } from "@/api/service/tool/gen/types";
import { Columns } from "@/api/types";

interface FormItem {
  title: string;
  dataIndex: string;
  tooltip?: string;
}
// eslint-disable-next-line import/prefer-default-export
export const formItems: FormItem[] = [
  {
    title: "表名称",
    dataIndex: "tableName",
    tooltip: "数据库表的名字"
  },
  {
    title: "表描述",
    dataIndex: "tableComment",
    tooltip: "对表的描述，不影响数据库"
  },
  {
    title: "实体类名称",
    dataIndex: "className",
    tooltip: "JAVA的类名"
  },
  {
    title: "作者",
    dataIndex: "functionAuthor"
  },
  {
    title: "备注",
    dataIndex: "remark",
    tooltip: "可选填"
  },
  {
    title: "包名",
    dataIndex: "packageName",
    tooltip: "生成java的包的路径和名字"
  },
  {
    title: "模块名",
    dataIndex: "moduleName",
    tooltip: "模块名+业务名+curd=权限"
  },
  {
    title: "业务名",
    dataIndex: "businessName",
    tooltip: "整个业务的名字，影响SQL,前端后端"
  },
  {
    title: "功能名",
    dataIndex: "functionName",
    tooltip: "影响数据库里面的数据，"
  }
];
export const formObject: GenInfo = {
  createBy: "",
  createTime: "",
  updateBy: "",
  tableId: 0,
  tableName: "",
  tableComment: "",
  className: "",
  tplCategory: "",
  tplWebType: "",
  packageName: "",
  moduleName: "",
  businessName: "",
  functionAuthor: "",
  genType: "",
  genPath: "",
  columns: [],
  sub: false,
  tree: false,
  crud: false
};
export const genColumns: Columns[] = [
  {
    title: "表名",
    dataIndex: "tableName",
    query: true,
    filedType: "input"
  },
  {
    title: "表描述",
    dataIndex: "tableComment",
    filedType: "input"
  },
  {
    title: "模块名",
    dataIndex: "moduleName",
    filedType: "input"
  },
  {
    title: "创建时间",
    dataIndex: "createTime",
    filedType: "input"
  }
];
