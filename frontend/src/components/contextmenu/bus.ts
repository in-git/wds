import { ref } from "vue";
import { ContextmenuConfig, ContextmenuData } from "./types";

export const contextmenuConfig = ref<ContextmenuConfig>({
  left: 0,
  top: 0,
  show: false,
  list: [],
  target: null,
  offsetX: 0,
  offsetY: 0,
  data: undefined
});
export const showContextmenu = (
  e: MouseEvent,
  list: ContextmenuData[],
  data: any,
  target?: Element,
  offsetX = 0,
  offsetY = 0
) => {
  e.preventDefault();
  contextmenuConfig.value.left = e.x;
  contextmenuConfig.value.top = e.y;
  contextmenuConfig.value.show = true;
  contextmenuConfig.value.list = list;
  contextmenuConfig.value.target = target;
  contextmenuConfig.value.data = data;
  contextmenuConfig.value.offsetY = offsetY;
  contextmenuConfig.value.offsetX = offsetX;
};
