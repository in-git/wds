export type ContextmenuData = {
  title: string;
  action: (...arg: any) => any;
  icon?: any;
  disabled?: boolean;
};
export type ContextmenuConfig<T = undefined> = {
  left: number;
  top: number;
  show: boolean;
  list: ContextmenuData[];
  /* 目标节点,右键菜单挂到这个位置 */
  target?: Element | null;
  offsetX?: number;
  offsetY?: number;
  /* 右键携带的数据 */
  data?: T;
};
