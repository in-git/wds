import { ref } from "vue";
import { TreeSelectorNode } from "./types";

// eslint-disable-next-line import/prefer-default-export
export const currentNode = ref<TreeSelectorNode>();
