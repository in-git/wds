export interface TreeSelectorNode {
  label: string;
  value: string;
  isExpand?: boolean;
  children: TreeSelectorNode[];
  deep?: number;
  checked?: boolean;
  nodeId?: string;
}
