export type SelectorType = "icon";

export type SelectorData =
  | {
      label: string;
      value: any;
    }
  | Record<string, any>;

export type SelectorProps = {
  data?: SelectorData[];
  multiple?: boolean;
  values: any[];
  type: SelectorType;
};
