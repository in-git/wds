import { ApiHooks, ApiMethod } from "@/api/types";

// eslint-disable-next-line import/prefer-default-export
export const runApiHooks = async (api: ApiHooks, method: ApiMethod) => {
  if (api[method]) {
    await api[method]();
  }
};
