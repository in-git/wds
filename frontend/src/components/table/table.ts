import { IQuery } from "@/api/types";
import { TableColumnData } from "@arco-design/web-vue";
import { ref } from "vue";

export const columns: TableColumnData[] = [];

export const loading = ref(false);
export const tableData = ref([]);

export const pageSizeChange = () => {};

export const query = ref<IQuery>({
  pageNum: 1,
  pageSize: 10
});
export const getTableData = () => {
  tableData.value = [];
};
