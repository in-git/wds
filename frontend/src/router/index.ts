import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "",
    component: () => import(/* webpackChunkName: "Home" */ "@/views/system/desktop/Desktop.vue")
  },
  {
    path: "/:catchAll(.*)",
    name: "404",
    component: () => import(/* webpackChunkName: "Home" */ "@/views/system/desktop/Desktop.vue")
  }
];

const router = createRouter({
  // history 模式,hash模式:createWebHashHistory()
  history: createWebHistory(),
  routes
});

export default router;
