import { ModuleEnum } from "@/data/modules";
import { TableColumnData, TableData, TreeFieldNames } from "@arco-design/web-vue";
import { FieldRule, ValidatedError } from "@arco-design/web-vue/es/form/interface";
/* 请求钩子函数方法 */
export type ApiMethod = "afterEdit" | "beforeCreate" | "beforeEdit" | "init";

export interface ApiHooks {
  [key: string]: () => any;
}

export type IOption = {
  label: string;
  value: string | number;
};
/* 查询条件 */
export type IQuery<T = Record<string, any>> = {
  pageNum?: number;
  pageSize?: number;
  /* 创建时间 */
  createTime?: string;
  isAsc?: "ascending" | "descending";
  /* 筛选状态 */
  status?: "0" | "1" | string;
  params?: {
    beginTime?: string;
    endTime?: string;
  };
  orderByColumn?: string;
  total?: number;
} & T;
/* 带分页数据列表的返回 */
export interface ITableResponse<T> {
  total: number;
  rows: T[];
  code: number;
  msg: string;
}
/* 拖拽相关的配置 */
export interface IDragData {
  /* 接收来自其他模块的数据 */
  accepts?: ModuleEnum[];
  ids?: any[];
  /* 在卡片中放下时对应的元素ID */
  targetId?: string;
  /* 在空白处放下 */
  dropInTable?: any;
  /* 在卡片中放下 */
  dropInCard?: any;
  /* 对于此次拖拽行位的描述,重要 */
  dropTips?: string;
}
/* 不分页的数据列表返回 */
export interface IDataResponse<T> {
  data: T[];
  msg: string;
  code: 200 | 500;
}
/* 统一反馈 */
export interface Feedback<T = undefined> {
  msg: string;
  code: string;
  data?: T;
}

export type FormEvent = {
  values: Record<string, any>;
  errors: Record<string, ValidatedError> | undefined;
};
/* 系统表单参数 */
export type Columns = {
  title: string;
  /* 索引 */
  dataIndex: string;
  /* 是否用于搜索 */
  query?: boolean;
  allowSort?: boolean;
  /* 表单输入类型，对应不同组件 */
  filedType?:
    | "input"
    | "select"
    | "boolean"
    | "number"
    | "textarea"
    | "radio"
    | "checkbox"
    | "datetime"
    | "imageUpload"
    | "fileUpload"
    | "editor"
    | "switch"
    | "password"
    | "treeSelect"
    | "icon-selector"
    | "tree";
  /* 下拉菜单的候选项 */
  option?: IOption[];
  /* 是否必填，表单校验 */
  isRequired?: boolean;
  /* 树形结构的选项 */
  treeOptions?: any[];
  /* 是否允许多选，在树形结构中使用 */
  multiple?: boolean;
  /* 是否禁用字段 */
  disabled?: boolean;
  /* 默认false，配置后不在列表中显示 */
  notColumn?: boolean;
  /* 自定义键，在渲染下拉菜单，属性结构时使用 */
  fieldNames?: TreeFieldNames;
  /* 限定规则 */
  rules?: FieldRule;
  /* 字段提示信息 */
  tips?: string;
} & Partial<TableColumnData>;

export type ConfigFilter = {
  /* 是否允许用时间进行搜索 */
  allowTimeQuery?: boolean;
  /* 是否开启状态筛选 */
  status?: boolean;
};
/* 增删改查的API */
export type ConfigApi = {
  /* 不用填，用于请求的初始化 */
  getList?: any;
  deleteList?: any;
  update?: any;
  create?: any;
  select?: any;
  list?: any;
  export?: any;
};
/* 表单的和兴配置选项 */
export interface IConfig<T = undefined> {
  table: {
    /* 表格加载 */
    loading: boolean;
    /* 表格数据 */
    tableData: TableData[];
    total: number;
    /* 表的ID，必须唯一 */
    rowKey: string;
    /* 已经选择的ID，实际值是[rowKey,...] */
    selectKeys: string[];
  };
  /* 在请求前，后触发的钩子函数，理解成切面，目前是临时解决方案，代码需要优化 */
  apiHooks?: Partial<Record<ApiMethod, () => any>>;
  /* 过滤筛选排序 */
  filter?: ConfigFilter;
  /* 请求的条件 */
  query: IQuery;
  /* API */
  api: ConfigApi;
  mode: "card" | "table";
  /* 用于卡片显示默认的标题，是JSON中的一个键 */
  cardKey: string;
  /* 模块名，用于存储表头 */
  moduleName: ModuleEnum;
  /* 显示编辑模态框 */
  showEditModal: boolean;
  /* 表头，表单的内容配置，核心选项，是a-table和a-form的结合体 */
  conditions: Columns[];
  form: {
    currentItem: T;
    /* 重置表单的方法 */
    raw: T;
    response: any;
  };
}
