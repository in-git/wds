import { getPageStore } from "@/store/page/utils";
import { getToken, setToken } from "@/store/user/utils";
import { requestHistory, responseHistory } from "@/views/layout/footer/logs/logs";
import { apps, missionList } from "@/views/system/desktop/desktop";
import { showLogin } from "@/views/system/login/login";
import { Message, Notification } from "@arco-design/web-vue";
import type { AxiosRequestConfig, AxiosResponse } from "axios";
import axios from "axios";

export interface HttpResponse<T = unknown> {
  status: number;
  msg: string;
  code: number;
  data: T;
}

export const setBaseUrl = (baseUrl?: string) => {
  if (baseUrl) {
    axios.defaults.baseURL = baseUrl;
  } else {
    const page = getPageStore();
    axios.defaults.baseURL = page.development.baseUrl;
  }
};

// if (import.meta.env.VITE_API_BASE_URL) {
//   axios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;
// }

axios.interceptors.request.use(
  (config: AxiosRequestConfig | any) => {
    const token = getToken();
    requestHistory.value.push({
      url: config.url,
      method: config.method,
      baseURL: config.baseURL,
      data: config.data
    });

    if (token) {
      if (!config.headers) {
        config.headers = {};
      }
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    // do something
    return Promise.reject(error);
  }
);

const logout = () => {
  setToken("");
  missionList.value = [];
  showLogin.value = true;
};
// add response interceptors
axios.interceptors.response.use(
  (response: AxiosResponse) => {
    const res = response.data;
    responseHistory.value.push({
      baseURL: response.config.baseURL!,
      data: res.data || res,
      method: response.config.method!,
      url: response.config.url!
    });
    if (res.code === 401) {
      apps.value = [];
      Message.error(res.msg);
      logout();
      return response;
    }
    if (res.code === 500) {
      Message.warning(res.msg || "系统遇到点问题");
      return Promise.reject(new Error(res.msg || "Error"));
    }
    return response;
  },
  (error) => {
    if (error.toString().includes("Network Error")) {
      Notification.warning({
        title: "网络错误",
        content: "请切换服务器"
      });
    }
    return Promise.reject(error);
  }
);
