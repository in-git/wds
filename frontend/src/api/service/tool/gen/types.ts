export interface Column {
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime?: any;
  remark?: any;
  columnId?: any;
  tableId: number;
  columnName?: any;
  columnComment?: any;
  columnType?: any;
  javaType?: any;
  javaField?: any;
  isPk?: any;
  isIncrement?: any;
  isRequired?: any;
  isInsert?: any;
  isEdit?: any;
  isList?: any;
  isQuery?: any;
  queryType?: any;
  htmlType?: any;
  dictType?: any;
  sort?: any;
  list: boolean;
  required: boolean;
  pk: boolean;
  insert: boolean;
  edit: boolean;
  usableColumn: boolean;
  superColumn: boolean;
  increment: boolean;
  query: boolean;
  capJavaField?: any;
}

export interface GenInfo {
  createBy?: any;
  createTime?: any;
  updateBy?: any;
  updateTime?: any;
  remark?: any;
  tableId: number;
  tableName: string;
  tableComment: string;
  subTableName?: any;
  subTableFkName?: any;
  className: string;
  tplCategory: string;
  tplWebType: string;
  packageName: string;
  moduleName: string;
  businessName: string;
  functionName?: any;
  functionAuthor: string;
  genType: string;
  genPath: string;
  pkColumn?: any;
  subTable?: any;
  columns: Column[];
  options?: any;
  treeCode?: any;
  treeParentCode?: any;
  treeName?: any;
  parentMenuId?: number;
  parentMenuName?: any;
  sub: boolean;
  tree: boolean;
  crud: boolean;
}

export interface Row {
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime?: any;
  remark?: any;
  columnId: number;
  tableId: number;
  columnName: string;
  columnComment?: any;
  columnType: string;
  javaType: string;
  javaField: string;
  isPk: string;
  isIncrement: string;
  isRequired: string;
  isInsert: string;
  isEdit?: string;
  isList?: string;
  isQuery?: string;
  queryType: string;
  htmlType: string;
  dictType: string;
  sort: number;
  list: boolean;
  required: boolean;
  pk: boolean;
  insert: boolean;
  edit: boolean;
  usableColumn: boolean;
  superColumn: boolean;
  increment: boolean;
  query: boolean;
  capJavaField: string;
}

export interface GenTable {
  createBy?: any;
  createTime?: any;
  updateBy?: any;
  updateTime?: any;
  remark?: any;
  tableId: number;
  tableName: string;
  tableComment: string;
  subTableName?: any;
  subTableFkName?: any;
  className: string;
  tplCategory: string;
  tplWebType: string;
  packageName: string;
  moduleName: string;
  businessName: string;
  functionName?: any;
  functionAuthor: string;
  genType?: any;
  genPath?: any;
  pkColumn?: any;
  subTable?: any;
  columns: Column[];
  options?: any;
  treeCode?: any;
  treeParentCode?: any;
  treeName?: any;
  parentMenuId?: any;
  parentMenuName?: any;
  sub: boolean;
  tree: boolean;
  crud: boolean;
}

export interface SelectGenResponse {
  tables: GenTable[];
  rows: Row[];
  info: GenInfo;
}
