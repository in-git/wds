import { Feedback, IDataResponse, IQuery, ITableResponse } from "@/api/types";
import axios from "axios";

import { GenInfo, GenTable, SelectGenResponse } from "./types";

export const listGen = (query?: IQuery) => {
  return axios.get("/tool/gen/list", { params: query });
};

export const selectGen = (id: string): Promise<IDataResponse<SelectGenResponse>> => {
  return axios.get(`/tool/gen/${id}`);
};

export const batchGenCode = (tables: string) => {
  return axios.get("/tool/gen/batchGenCode", {
    params: {
      tables
    },
    responseType: "blob" // 设置响应类型为blob
  });
};
export const updateGen = (data: GenInfo) => {
  return axios.put<Feedback>("tool/gen", data);
};

export const delGen = (id: string) => {
  return axios.delete<Feedback>(`tool/gen/${id}`);
};
/* 获取数据库的列表 */
export const listDb = (query: IQuery) => {
  return axios.get<ITableResponse<GenTable>>("tool/gen/db/list", {
    params: query
  });
};
export const importTable = (tables: string[]) => {
  return axios.post<Feedback>(`/tool/gen/importTable?tables=${tables.join(",")}`);
};
