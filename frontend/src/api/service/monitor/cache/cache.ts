import { Feedback } from "@/api/types";
import axios from "axios";
import { MonitorCache } from "./types";

// eslint-disable-next-line import/prefer-default-export
export const monitorCache = () => {
  return axios.get<Feedback<MonitorCache>>("monitor/cache");
};
