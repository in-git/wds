import { Feedback, IQuery } from "@/api/types";
import axios from "axios";
import { OperLog } from "./types";

export const listOperLog = (query: IQuery) => {
  return axios.get<Feedback<OperLog>>("/monitor/operlog/list", {
    params: query
  });
};

export const deleteOperLog = (ids: string[]) => {
  return axios.delete<Feedback<OperLog>>(`/monitor/operlog/${ids.join(",")}`);
};
