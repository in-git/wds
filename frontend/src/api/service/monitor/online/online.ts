import { Feedback, IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import { Online } from "./types";

export const listOnline = (query: IQuery) => {
  return axios.get<ITableResponse<Online>>("/monitor/online/list", {
    params: query
  });
};
/* 强退用户 */
export const forceLogout = (tokenId: string) => {
  return axios.delete<Feedback>(`/monitor/online/${tokenId}`);
};
