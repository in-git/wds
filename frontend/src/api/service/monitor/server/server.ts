import { Feedback } from "@/api/types";
import axios from "axios";
import { MonitorServer } from "./types";

// eslint-disable-next-line import/prefer-default-export
export const monitorServer = () => {
  return axios.get<Feedback<MonitorServer>>("/monitor/server");
};
