import { Feedback, IQuery } from "@/api/types";
import axios from "axios";
import { LoginInfo } from "./types";

export const listLoginInfo = (query: IQuery) => {
  return axios.get<Feedback<LoginInfo>>("/monitor/logininfor/list", {
    params: query
  });
};
export const deleteLoginInfo = (id: string) => {
  return axios.delete<Feedback<LoginInfo>>(`/monitor/logininfor/${id}`);
};
