import { IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import { Job } from "./types";

// eslint-disable-next-line import/prefer-default-export
export const listJobs = (query: IQuery) => {
  return axios.get<ITableResponse<Job>>("/monitor/job/list", {
    params: query
  });
};
