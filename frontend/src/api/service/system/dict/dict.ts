import { Feedback, IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import { Dict } from "./types";

export const listDict = (query?: IQuery) => {
  return axios.get<ITableResponse<Dict>>("/system/dict/type/list/", {
    params: query
  });
};

export const updateDict = (dict: Dict) => {
  return axios.put<Feedback>("/system/dict/type", dict);
};

export const createDict = (dict: Dict) => {
  return axios.post<Feedback>("/system/dict/type", dict);
};

export const deleteDict = (ids: string[]) => {
  return axios.delete<Feedback>(`/system/dict/type/${ids.join(",")}`);
};

export const selectDict = (id: string) => {
  return axios.get<Feedback<Dict>>(`/system/dict/type/${id}`);
};
