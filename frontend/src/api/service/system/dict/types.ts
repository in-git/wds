export interface Dict {
  createBy: string;
  createTime: string;
  updateBy?: any;
  updateTime?: any;
  remark: string;
  dictId: number;
  dictName: string;
  dictType: string;
  status: string;
}
