import { Feedback, IDataResponse, IQuery } from "@/api/types";
import axios from "axios";
import { IMenu, RoleMenuResponse, RouterChild } from "./types";

/* 获取身份对应的菜单，和已经选择了的菜单 */
export const roleMenuTreeSelect = (id: string) => {
  return axios.get<RoleMenuResponse>(`/system/menu/roleMenuTreeselect/${id}`);
};

export const menuTreeSelect = async () => {
  return axios.get<Feedback<IMenu[]>>(`/system/menu/treeselect/`);
};

export const getRouters = () => {
  return axios.get<IDataResponse<RouterChild>>("/getRouters");
};
export const createMenu = (data: IMenu) => {
  return axios.post<Feedback>("/system/menu", data);
};

export const listMenu = (query?: IQuery) => {
  return axios.get<IDataResponse<IMenu>>(`/system/menu/list/`, {
    params: query
  });
};

export const selectMenu = (id: string) => {
  return axios.get<Feedback>(`/system/menu/${id}`);
};
export const updateMenu = (data: IMenu) => {
  return axios.put<Feedback>("/system/menu", data);
};

export const deleteMenus = (ids: string[]) => {
  return axios.delete<Feedback>(`/system/menu/${ids.join(",")}`);
};
