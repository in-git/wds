export interface IMenu {
  createBy?: any;
  createTime: string;
  updateBy?: any;
  updateTime?: any;
  remark?: any;
  menuId: string;
  menuName: string;
  parentName?: any;
  parentId: string;
  orderNum: number;
  path: string;
  component?: any;
  query: string;
  isFrame: string;
  isCache: string;
  menuType: "M" | "F" | "C";
  visible: string;
  status: string;
  perms: string;
  icon: string;
  children: IMenu[] | undefined;
}
export interface Meta {
  title?: string;
  icon?: string;
  noCache?: boolean;
  link?: string;
}

export interface RouterChild {
  name: string;
  path: string;
  hidden: boolean;
  redirect?: string;
  component: string;
  alwaysShow?: boolean;
  meta?: Meta;
  children?: RouterChild[];
  show?: boolean;
}
export interface RoleMenuResponse {
  msg: string;
  code: number;
  menus: IMenu[];
  checkedKeys: string[];
}
