import { Feedback, IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import { Shortcut } from "./types";

// 查询shortcut列表
export const listShortcut = (query?: IQuery) => {
  return axios.get<ITableResponse<Shortcut>>(`/service/shortcut/list`, {
    params: query
  });
};

// 查询shortcut详细
export const getShortcut = (id: string) => {
  return axios.get<ITableResponse<Shortcut>>(`/service/shortcut/${id}`);
};

// 新增shortcut
export const createShortcut = (data: Shortcut) => {
  return axios.post<Feedback>(`/service/shortcut`, data);
};

// 修改shortcut
export const updateShortcut = (data: Shortcut) => {
  return axios.put<Feedback>(`/service/shortcut`, data);
};

// 删除shortcut
export const delShortcut = (id: string) => {
  return axios.delete<Feedback>(`/service/shortcut/${id}`);
};
