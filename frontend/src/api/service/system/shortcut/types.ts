export interface Shortcut {
  createBy?: any;
  createTime?: string;
  updateBy?: any;
  updateTime?: any;
  remark?: any;
  id?: number;
  name: string;
  path: string;
  userId: number;
  sort?: any;
  link?: any;
  title?: any;
  type: "1" | "0";
}
