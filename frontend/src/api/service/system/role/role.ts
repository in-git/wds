import { Feedback, IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import { ref } from "vue";
import { IUserInfo } from "../user/types";
import { Role } from "./types";

export const listRoles = (query?: Partial<IQuery>) => {
  return axios.get<ITableResponse<Role>>("/system/role/list", {
    params: query
  });
};

export const updateRole = (role: Role) => {
  return axios.put<ITableResponse<Feedback>>("/system/role", role);
};

export const selectRole = (id: string) => {
  return axios.get<Feedback<Role>>(`/system/role/${id}`);
};
export const createRole = (role: Role) => {
  return axios.post<Feedback>("/system/role", role);
};
export const deleteRole = (ids: string[]) => {
  return axios.delete<Feedback>(`/system/role/${ids.join(",")}`);
};

export const allocatedList = (query: IQuery<Partial<Role>>) => {
  return axios.get<ITableResponse<IUserInfo>>(`/system/role/authUser/allocatedList`, {
    params: query
  });
};
/* 分配用户 */
export const roleAuthUser = (roleId: string, userIds: number[]) => {
  const params = new URLSearchParams();
  params.append("roleId", roleId);
  params.append("userIds", userIds.join(","));

  return axios.put<Feedback>(`/system/role/authUser/selectAll/?${params.toString()}`);
};
export const checkedKeys = ref<string[]>([]);
