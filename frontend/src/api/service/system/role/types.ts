export interface Role {
  createBy?: any;
  createTime?: any;
  updateBy?: any;
  updateTime?: any;
  remark?: any;
  roleId: string;
  roleName: string;
  roleKey: string;
  roleSort: number;
  dataScope: string;
  menuCheckStrictly: boolean;
  deptCheckStrictly: boolean;
  status: "0" | "1";
  delFlag?: any;
  flag: boolean;
  menuIds?: any;
  deptIds?: any;
  permissions?: any;
  admin: boolean;
}
