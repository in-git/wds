import { IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import { SystemConfig } from "./types";

export const listSystemConfig = (query: IQuery) => {
  return axios.get<ITableResponse<SystemConfig>>("/system/config/list", {
    params: query
  });
};

export const selectSystemConfig = (id: string) => {
  return axios.get<ITableResponse<SystemConfig>>(`/system/config/${id}`);
};

export const createSystemConfig = (data: SystemConfig) => {
  return axios.post("/system/config", data);
};

export const updateSystemConfig = (data: SystemConfig) => {
  return axios.put(`/system/config/`, data);
};

export const deleteSystemConfig = (ids: string[]) => {
  return axios.delete(`/system/config/${ids.join(",")}`);
};
