export interface SystemConfig {
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime?: any;
  remark: string;
  configId: number;
  configName: string;
  configKey: string;
  configValue: string;
  configType: string;
}
