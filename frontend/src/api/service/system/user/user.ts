import { Feedback, IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import {
  CaptchaResponse,
  IUserInfo,
  LoginParams,
  LoginResponse,
  SystemUserResponse,
  UserIdentity,
  UserInfoResponse
} from "./types";

export const captchaImage = () => {
  return axios.get<CaptchaResponse>("/captchaImage");
};

export const login = (data: LoginParams) => {
  return axios.post<LoginResponse>("/login", data);
};

export const getInfo = () => {
  return axios.get<UserInfoResponse>("/getInfo");
};
/* 更新用户信息 */
export const updateInfo = (info: IUserInfo) => {
  return axios.put<UserInfoResponse>("/system/user/profile", info);
};

export const listUser = (query?: IQuery) => {
  return axios.get<ITableResponse<IUserInfo>>("/system/user/list", {
    params: query
  });
};

/* 获取 */
export const systemUser = () => {
  return axios.get<SystemUserResponse>(`/system/user/`);
};

/* 获取 */
export const deleteUser = (ids: string[]) => {
  return axios.delete<SystemUserResponse>(`/system/user/${ids.join(",")}`);
};

export const selectUser = (id = "") => {
  return axios.get<UserIdentity>(`/system/user/${id}`);
};
export const updateUser = (info: IUserInfo) => {
  return axios.put<Feedback>("system/user", info);
};
export const createUser = (info: IUserInfo) => {
  return axios.post<Feedback>("system/user", info);
};

export const updatePassword = (config: { password: string; userId: string }) => {
  return axios.put<Feedback>("system/user/resetPwd", config);
};
export const exportFile = () => {
  return axios.post<Blob>("/system/user/export", null, {
    responseType: "blob"
  });
};
