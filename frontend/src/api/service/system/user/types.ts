import { Dept } from "../dept/types";
import { IPost } from "../post/types";
import { Role } from "../role/types";

export type LoginParams = {
  code: string;
  password: string;
  username: string;
  uuid: string;
};

export interface CaptchaResponse {
  captchaEnabled: boolean;
  code: number;
  img: string;
  msg: string;
  uuid: string;
}
export interface LoginResponse {
  msg: string;
  code: string;
  token: string;
}

export interface IUserInfo {
  createBy: string;
  createTime: string;
  updateBy?: any;
  updateTime?: any;
  remark: string;
  userId: string;
  deptId: string;
  userName: string;
  nickName: string;
  email: string;
  phonenumber: string;
  sex: string;
  avatar: string;
  password: string;
  status: string;
  delFlag: string;
  loginIp: string;
  loginDate: string;
  dept?: Dept;
  roles: Role[];
  roleIds?: string[];
  postIds: string[];
  roleId?: string[];
  admin: boolean;
}

export interface UserInfoResponse {
  msg: string;
  code: number;
  permissions: string[];
  roles: string[];
  user: IUserInfo;
}

export interface SystemUserResponse {
  msg: string;
  code: number;
  roles: Role[];
  posts: IPost[];
}
export interface UserIdentity {
  msg: string;
  code: number;
  roleIds: string[];
  data: IUserInfo;
  postIds: string[];
  roles: Role[];
  posts: IPost[];
}
