import { Feedback, IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import { Notice } from "./types";

export const listNotice = (query?: IQuery) => {
  return axios.get<ITableResponse<Notice>>("/system/notice/list", {
    params: query
  });
};

export const createNotice = (notice: Notice) => {
  return axios.post<Feedback>("/system/notice/", notice);
};

export const updateNotice = (notice: Notice) => {
  return axios.put<Feedback>("/system/notice/", notice);
};

export const deleteNotice = (ids: string[]) => {
  return axios.delete<Feedback>(`/system/notice/${ids.join(",")}`);
};

export const selectNotice = (id: string) => {
  return axios.get<Feedback>(`/system/notice/${id}`);
};
