import { Feedback, IDataResponse, IQuery } from "@/api/types";
import axios from "axios";
import { Dept } from "./types";

export const deptTree = () => {
  return axios.get<IDataResponse<Dept>>("/system/user/deptTree");
};

export const listDept = (query: IQuery) => {
  return axios.get<IDataResponse<Dept>>("/system/dept/list/", query);
};

export const updateDept = (dept: Dept) => {
  return axios.put<Feedback>("/system/dept", dept);
};

export const createDept = (dept: Dept) => {
  return axios.post<Feedback>("/system/dept", dept);
};

export const deleteDept = (ids: string[]) => {
  return axios.delete<Feedback>(`/system/dept/${ids.join(",")}`);
};

export const selectDept = (id: string) => {
  return axios.get<Feedback<Dept>>(`/system/dept/${id}`);
};
