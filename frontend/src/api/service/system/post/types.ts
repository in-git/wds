export interface IPost {
  createBy: string;
  createTime: string;
  updateBy?: any;
  updateTime?: any;
  remark: string;
  postId: string;
  postCode: string;
  postName: string;
  postSort: number;
  status: string;
  flag: boolean;
}
