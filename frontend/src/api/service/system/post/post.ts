import { Feedback, IQuery, ITableResponse } from "@/api/types";
import axios from "axios";
import { IPost } from "./types";

export const listPost = (query?: IQuery) => {
  return axios.get<ITableResponse<IPost>>("/system/post/list", {
    params: query
  });
};

export const createPost = (post: IPost) => {
  return axios.post<ITableResponse<IPost>>("/system/post", post);
};

export const updatePost = (post: IPost) => {
  return axios.put<ITableResponse<IPost>>("/system/post", post);
};

export const selectPost = (id: string) => {
  return axios.get<Feedback<IPost>>(`/system/post/${id}`);
};

export const deletePost = (ids: string[]) => {
  return axios.delete<Feedback<IPost>>(`/system/post/${ids.join(",")}`);
};
