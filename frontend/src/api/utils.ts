import { getToken } from "@/store/user/utils";
import axios from "axios";

interface UploadResponse {
  msg: string;
  imgUrl: string;
  code: number;
}
// eslint-disable-next-line import/prefer-default-export
export const uploadFile = (url: string, file: File) => {
  const token = getToken();
  const formData = new FormData();
  formData.append("avatarfile", file);
  return axios.post<UploadResponse>(url, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${token}`
    }
  });
};

export async function downloadSystemFile(blob: Blob, fileName: string) {
  const blobUrl = URL.createObjectURL(blob);

  const downloadLink = document.createElement("a");
  downloadLink.href = blobUrl;
  downloadLink.download = fileName;

  // 模拟点击下载链接
  document.body.appendChild(downloadLink);
  downloadLink.click();

  // 清理
  document.body.removeChild(downloadLink);
  URL.revokeObjectURL(blobUrl);
}
