import { nextTick } from "vue";

export default {
  created(el: HTMLElement) {
    el.style.userSelect = "none";
    el.style.cursor = "move";
    const offset = {
      x: 0,
      y: 0
    };
    nextTick(() => {
      const parent = el.parentElement as HTMLElement;
      const domRect = parent.getBoundingClientRect();
      el.onmousedown = (de: MouseEvent) => {
        let offsetX = de.x;
        let offsetY = de.y;
        const onmousemove = (me: MouseEvent) => {
          offset.x += me.x - offsetX;
          offset.y += me.y - offsetY;
          offsetX = me.x;
          offsetY = me.y;

          parent.style.transform = `translate(${offset.x}px,${offset.y}px)`;
        };
        document.addEventListener("mousemove", onmousemove);

        const mouseup = () => {
          document.removeEventListener("mouseup", mouseup);
          document.removeEventListener("mousemove", onmousemove);
          const minLeft = -domRect.width - 30;
          const maxLeft = domRect.width - 30;
          const maxTop = domRect.height;
          if (offset.x >= maxLeft) {
            offset.x = maxLeft;
          }
          if (offset.x < minLeft) {
            offset.x = -200;
          }
          if (offset.y < 0) {
            offset.y = 0;
          }
          if (offset.y >= maxTop) {
            offset.y = maxTop - 48;
          }
          parent.style.transform = `translate(${offset.x}px,${offset.y}px)`;
        };
        document.addEventListener("mouseup", mouseup);
      };
    });
  }
};
