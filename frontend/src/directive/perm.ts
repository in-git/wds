// permission.ts

import { getUserInfo } from "@/data/cache";
import { DirectiveBinding } from "vue";

export default {
  async mounted(el: HTMLElement, binding: DirectiveBinding) {
    const { value } = binding;

    const userInfo = await getUserInfo();
    if (!userInfo) {
      return;
    }
    if (userInfo.permissions.includes("*:*:*")) {
      return;
    }
    if (!userInfo.permissions.includes(value)) {
      el.style.display = "none";
    }
  }
};
