/*

鼠标拖拽的效果
使用
     <div
        v-dragEffect
        data-drag-effect
      />
*/
export default {
  created(el: HTMLElement) {
    el.ondragover = (e) => {
      el?.classList.add("drag-effect");
      e.preventDefault();
    };
    el.ondragenter = () => {
      el?.classList.add("drag-effect");
    };

    el.ondrop = () => {
      el?.classList.remove("drag-effect");
    };
  }
};
