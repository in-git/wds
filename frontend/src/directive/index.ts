import { App } from "vue";
import dragBox from "./dragBox";
import dragEffect from "./dragEffect";
import draggable from "./draggable";
import focus from "./focus";
import perm from "./perm";

export default {
  install(Vue: App) {
    Vue.directive("draggable", draggable);
    Vue.directive("focus", focus);
    Vue.directive("dragEffect", dragEffect);
    Vue.directive("dragBox", dragBox);
    Vue.directive("perm", perm);
  }
};
