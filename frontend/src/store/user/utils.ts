import useUserStore from "./user";

export const setToken = (token: string) => {
  const store = useUserStore();
  store.$state.token = token;
};

export const getToken = () => {
  const store = useUserStore();
  return store.$state.token;
};

export const getUserStore = () => {
  return useUserStore().$state;
};
