export type UserAuth = {
  password: string;
  username: string;
};
export interface UserStore {
  token?: string;
  users: UserAuth[];
}
