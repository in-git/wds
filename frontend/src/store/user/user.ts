import { defineStore } from "pinia";
import { UserStore } from "./types";

const useUserStore = defineStore("user", {
  state: (): UserStore => ({
    token: "",
    users: [
      {
        password: "admin123",
        username: "admin"
      }
    ]
  }),
  persist: true
});

export default useUserStore;
