import { RouterChild } from "@/api/service/system/menu/types";

export type paginationPosition = "tl" | "top" | "tr" | "bl" | "bottom" | "br";

export type Page = {
  /* 全局配置 */
  size: "medium" | "large" | "small" | "mini";
  theme: "light" | "dark";
  scrollToClose: boolean;
  updateAtScroll: boolean;
  win: {
    size: string;
  };
  /* 自定义组件配置 */
  stickyHeader: boolean;
  table: {
    cell: boolean;
    headerCell: boolean;
    wrapper: boolean;
    bodyCell: boolean;
    hoverable: boolean;
    stripe: boolean;
    showHeader: boolean;
    stickyHeader: boolean;
    tableLayoutFixed: boolean;
    columnResizable: boolean;
  };
  pagination: {
    position: paginationPosition;
    showMore: boolean;
    simple: boolean;
    showJumper: boolean;
    showTotal: boolean;
  };
  desktop: {
    background: string;
  };
  development: {
    baseUrl: string;
    urlList: string[];
  };
  shortcut: Partial<RouterChild>[];
};
