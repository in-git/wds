import { defineStore } from "pinia";
import { Page } from "./types";

const usePageStore = defineStore("page", {
  state: (): Page => ({
    win: {
      size: "900,700"
    },
    size: "medium",
    theme: "light",
    scrollToClose: false,
    updateAtScroll: false,
    stickyHeader: false,
    pagination: {
      position: "br",
      showMore: false,
      showTotal: false,
      showJumper: false,
      simple: false
    },
    table: {
      hoverable: false,
      showHeader: true,
      stickyHeader: false,
      tableLayoutFixed: false,
      columnResizable: false,
      stripe: false,
      cell: false,
      headerCell: false,
      wrapper: false,
      bodyCell: false
    },
    desktop: {
      background: ""
    },
    development: {
      baseUrl: "http://150.158.14.110:8081/",
      urlList: ["http://150.158.14.110:8081/", "http://vue.ruoyi.vip/prod-api"]
    },
    shortcut: []
  }),
  persist: true
});

export default usePageStore;
