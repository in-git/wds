import usePageStore from "./page";

export const getPageStore = () => {
  return usePageStore().$state;
};
export default {
  getPageStore
};
