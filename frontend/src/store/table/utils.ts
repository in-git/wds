import { ModuleEnum } from "@/data/modules";
import { Message } from "@arco-design/web-vue";
import useTableStore, { StoreColumn } from "./table";

export const getTableStore = () => {
  return useTableStore().$state;
};
export const triggerColumn = (module: string, column: StoreColumn) => {
  const columns = getTableStore();
  columns.tableColumns = columns.tableColumns.filter((e) => {
    if (e.moduleName === module) {
      /* 判断是否存在 */
      const target = e.columns.find((v) => {
        if (v.dataIndex === column.dataIndex) {
          return v;
        }
        return undefined;
      });
      if (target) {
        if (e.columns.length === 1) {
          Message.warning("最后一项，不允许删除");
        } else {
          e.columns = e.columns.filter((v) => {
            return v.dataIndex !== column.dataIndex;
          });
        }
      } else {
        e.columns.unshift(column);
      }
    }
    return e;
  });
};
export const resetColumns = (module: ModuleEnum) => {
  const columns = getTableStore();
  columns.tableColumns = columns.tableColumns.filter((e) => {
    if (e.moduleName === module) {
      e.columns = [];
    }
    return e;
  });
};
