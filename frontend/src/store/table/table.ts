import { TableColumnData } from "@arco-design/web-vue";
import { defineStore } from "pinia";

export type StoreColumn = {
  title: string;
  dataIndex: string;
} & Partial<TableColumnData>;

type TableColumn = {
  columns: StoreColumn[];
  moduleName: string;
} & Partial<TableColumnData>;
const useTableStore = defineStore("table", {
  state: (): {
    tableColumns: TableColumn[];
  } => {
    return {
      tableColumns: []
    };
  },
  persist: true
});

export default useTableStore;
