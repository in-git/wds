import { ApiMethod, Columns, ConfigApi, ConfigFilter, IConfig, IDragData } from "@/api/types";
import { Ref, ref } from "vue";
import { ModuleEnum } from "./modules";

export type ConfigParams<T = any> = {
  rowKey: string;
  cardKey: string;
  moduleName: ModuleEnum;
  api: ConfigApi;
  apiHooks?: Partial<Record<ApiMethod, () => any>>;
  form: T;
  /* 表头 */
  columns: Columns[];
  mode?: "table" | "card";
  filter?: ConfigFilter;
  drop?: IDragData;
};
export const getConfig = (params: ConfigParams): Ref<IConfig<any>> => {
  const config: IConfig = {
    table: {
      loading: false,
      tableData: [],
      total: 0,
      rowKey: params.rowKey,
      selectKeys: []
    },
    filter: params.filter,
    query: {
      // createTime: "",
      // status: "",
      // params: {},
      // isAsc: "ascending"
      pageNum: 1,
      pageSize: 10
    },
    apiHooks: params.apiHooks,
    api: {
      ...params.api
    },
    mode: params.mode || "table",
    cardKey: params.cardKey,
    moduleName: params.moduleName,
    showEditModal: false,
    conditions: params.columns,
    form: {
      currentItem: params.form,
      raw: params.form,
      response: undefined
    }
  };
  return ref({
    ...config
  });
};
