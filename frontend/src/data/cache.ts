import { deptTree } from "@/api/service/system/dept/dept";
import { Dept } from "@/api/service/system/dept/types";
import { listMenu } from "@/api/service/system/menu/menu";
import { IMenu } from "@/api/service/system/menu/types";
import { listPost } from "@/api/service/system/post/post";
import { UserInfoResponse } from "@/api/service/system/user/types";
import { getInfo } from "@/api/service/system/user/user";
import { getToken } from "@/store/user/utils";
import { arrayToTree } from "@/utils/data";
import { ref } from "vue";

/* 部门 */
export const deptTreeData = ref<Dept[]>([]);
/* 树形菜单 */

export const getDept = async () => {
  const { data } = await deptTree();
  return data.data || [];
};
export const userInfo = ref<UserInfoResponse>();
/* 

   hasParent: 渲染菜单，确定是否有根目录 
   forceUpdate:清空之前的缓存，强制刷新
  
  */
export const getMenuTree = async (hasParent = true, forceUpdate = false) => {
  let menuTree: IMenu[] = [];
  if (forceUpdate) {
    menuTree = [];
  }
  const parentMenu: IMenu = {
    createTime: "",
    menuId: "0",
    menuName: "主目录",
    parentId: "0",
    orderNum: 0,
    path: "",
    query: "",
    isFrame: "",
    isCache: "",
    menuType: "M",
    visible: "",
    status: "",
    perms: "",
    icon: "",
    children: []
  };
  if (menuTree.length === 0) {
    const { data } = await listMenu();
    if (hasParent) {
      parentMenu.children = arrayToTree(data.data, "0");
      menuTree = [parentMenu];
    } else {
      menuTree = arrayToTree(data.data, "0");
      menuTree.unshift(parentMenu);
    }
  }
  return menuTree;
};
export const getPost = async () => {
  const { data } = await listPost();
  return data.rows || [];
};

/* forceUpdate：强制刷新用户信息 */
export const getUserInfo = async (forceUpdate = false) => {
  const token = getToken();
  if (!token) return userInfo.value;
  if (!userInfo.value || forceUpdate) {
    const { data } = await getInfo();
    userInfo.value = data;
  }

  return userInfo.value;
};
