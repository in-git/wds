// 各个模块的别名
// eslint-disable-next-line import/prefer-default-export, no-shadow
export enum ModuleEnum {
  ROLE = "ROLE",
  USER = "USER",
  POST = "POST",
  NOTICE = "NOTICE",
  MENU = "MENU",
  DICT = "DICT",
  DEPT = "DEPT",
  CONFIG = "CONFIG",
  OPERLOG = "OPERLOG",
  ONLINE = "ONLINE",
  JOB = "JOB",
  GEN = "GEN",
  LOGIN_INFO = "LOGIN_INFO",
  DESKTOP_SETTING = "DESKTOP_SETTING"
}
