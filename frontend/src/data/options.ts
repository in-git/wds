export const isOrNotOptions = [
  {
    label: "是",
    value: "0"
  },
  {
    label: "否",
    value: "1"
  }
];
export const statusOptions = [
  {
    label: "启用",
    value: "0"
  },
  {
    label: "禁用",
    value: "1"
  }
];
