/* eslint-disable no-restricted-syntax */
import { IMenu } from "@/api/service/system/menu/types";

export const arrayToTree = (menuArray: IMenu[], parentId: string | null = null): IMenu[] => {
  const menuTree: IMenu[] = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const menu of menuArray) {
    if (`${menu.parentId}` === `${parentId}`) {
      menu.menuId = `${menu.menuId}`;
      const children = arrayToTree(menuArray, menu.menuId);
      if (children.length !== 0) {
        menu.children = children;
      } else {
        menu.children = undefined;
      }
      menuTree.push(menu);
    }
  }
  return menuTree;
};

export const formatPercentage = (value: number, decimalPlaces = 2): number => {
  return Number(value.toFixed(decimalPlaces));
};

/* 获取完整路径 */
