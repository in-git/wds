/* eslint-disable no-restricted-syntax */
import { RouterChild } from "@/api/service/system/menu/types";
import { getPageStore } from "@/store/page/utils";
import { createMission } from "@/views/system/desktop/desktop";
import { TreeData } from "@/views/system/desktop/folder/tree/types";
import { systemRouters } from "@/views/system/userCenter/userCenter";
import { useCloned } from "@vueuse/core";
import { defineAsyncComponent, markRaw, ref } from "vue";
import { openLink } from "./utils";

type DynamicComponent = {
  component: any;
  path: string;
  componentName: string;
  name: string;
};
export const avatar = ref();
/* 读取整个文件夹下的内容 */
export const allModules = ref<DynamicComponent[]>([]);

export const loadAllModules = async () => {
  // 遍历上下文中的每个组件文件
  const modules = import.meta.glob("@/views/modules/**/index.vue");
  Object.entries(modules).forEach(([path, asyncCom]: any) => {
    const name = path.replace(/\.\/components-new\/(.*)\.vue/, "$1");
    const pathArr = path.split("/");
    allModules.value.push({
      component: markRaw(defineAsyncComponent(asyncCom)),
      path,
      name: pathArr[pathArr.length - 2],
      componentName: name
    });
  });
};

/* 关闭文件夹 */
export const closeFolder = (tree: TreeData[], name: string) => {
  tree.forEach((e) => {
    if (e.name === name) {
      e.show = true;
      return;
    }
    if (e.children && e.children.length > 0) {
      closeFolder(e.children, name);
    }
  });
};
/* 获取完整路径 */
function getFullPath(tree: RouterChild[], target: string): string | null {
  for (const node of tree) {
    const fullPath = `${node.path}/`;

    if (node.path === target) {
      return fullPath;
    }

    if (node.children && node.children.length > 0) {
      const childPath = getFullPath(node.children, target);
      if (childPath !== null) {
        return `${fullPath}${childPath}`;
      }
    }
  }

  return null;
}
/* 打开文件夹 */
export const openMenu = (item: RouterChild) => {
  if (item.meta && item.meta.link) {
    openLink(item.meta.link);
  }

  const target = allModules.value.find((e) => {
    const fullPath = getFullPath(systemRouters.value, item.path);
    if (fullPath && e.path.includes(fullPath)) {
      return e;
    }
    // if (getDirectoryName(e.path, item.path) && e.path.includes(item.path)) {
    //   return e;
    // }
    return null;
  });

  if (!target || (item.children && item.children.length > 0)) return;
  // getFullPath(target.path);

  createMission({
    name: item.meta?.title || "无标题",
    component: target.component,
    attrs: {
      routers: useCloned(systemRouters.value).cloned.value
    }
  });
};
export const getAvatar = (suffix: string) => {
  const page = getPageStore();
  const host = page.development.baseUrl;

  return host + suffix.substring(1, suffix.length);
};
