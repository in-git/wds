import ArcoVue from "@arco-design/web-vue/es/arco-vue";
import ArcoVueIcon from "@arco-design/web-vue/es/icon";
import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import { createApp } from "vue";
import App from "./App.vue";
import directive from "./directive";
import router from "./router";

import "@/assets/style/index.scss";
import "@arco-design/web-vue/dist/arco.css";
import "in-less";
import "./api/interceptor";

const app = createApp(App);
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);
app.use(ArcoVueIcon);
app.use(pinia);
app.use(directive);
app.use(router);
app.use(ArcoVue);
app.mount("#app");
